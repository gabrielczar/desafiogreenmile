package com.gabrielczar.desafiogreenmile.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gabrielczar.desafiogreenmile.models.AccountCredentialsModel;
import com.gabrielczar.desafiogreenmile.models.AccountWrapper;
import com.gabrielczar.desafiogreenmile.repositories.AccountRepository;
import com.gabrielczar.desafiogreenmile.utils.UserUtils;
import com.jayway.jsonpath.JsonPath;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@Ignore
@SpringBootTest
@RunWith(SpringRunner.class)
public class AbstractMvcTest {
    private final Logger logger = Logger.getLogger(this.getClass().getName());

    private ObjectMapper mapper = new ObjectMapper();
    private static Set<Class> inited = new HashSet<>();

    protected MockMvc mockMvc;

    protected UserUtils userUtils;

    protected final String username = "username", password = "password";

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private AccountRepository accountRepository;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).apply(springSecurity()).build();
        userUtils = new UserUtils(mockMvc);
    }


    @Before
    public void init() throws Exception {
        if (!inited.contains(getClass())) {
            doInit();
            inited.add(getClass());
        }
    }

    protected void doInit() throws Exception {
    }

    protected String json(Object o) throws IOException {
        return mapper.writeValueAsString(o);
    }


    protected ResultActions login(String username, String password) throws Exception {
        final AccountWrapper auth = new AccountWrapper();
        auth.setUsername(username);
        auth.setPassword(password);

        return mockMvc.perform(
                post("/api/public/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json(auth)));
    }

    protected String extractToken(MvcResult result) throws UnsupportedEncodingException {
        return JsonPath.read(result.getResponse().getContentAsString(), "$.token");
    }

    protected void clearData() {
        AccountCredentialsModel acc = accountRepository.findByUsername(username);
        if (acc != null)
            accountRepository.delete(acc);
    }
}
