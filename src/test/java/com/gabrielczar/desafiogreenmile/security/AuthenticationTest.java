package com.gabrielczar.desafiogreenmile.security;

import com.gabrielczar.desafiogreenmile.enumerations.Authority;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class AuthenticationTest extends AbstractMvcTest {

    @Override
    protected void doInit() throws Exception {
        this.clearData();
        this.userUtils.registerUser(username, password, Authority.PARTICIPANTE)
                .andExpect(status().isCreated());
    }

    @Test
    public void loginInvalidRequest() throws Exception {
        this.mockMvc.perform(get("/api/public/login"))
                .andExpect(status().is4xxClientError());
    }


    @Test
    public void loginNok() throws Exception {
        login(username, username).andExpect(status().is4xxClientError());
    }


    @Test
    public void loginOk() throws Exception {
        login(username, password)
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.token").exists())
                .andExpect(jsonPath("$.account.username", equalTo(username)))
                .andExpect(jsonPath("$.account.password").doesNotExist())
                .andReturn();
    }

    @Test
    public void userRepositoryWithTokenIsAllowed() throws Exception {
        final String token = extractToken(login(username, password).andReturn());
        mockMvc.perform(get("/api/home").header("Authorization", "Bearer " + token))
                .andDo(print())
                .andExpect(status().isOk());
    }

}

