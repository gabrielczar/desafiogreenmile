package com.gabrielczar.desafiogreenmile.controllers;

import com.gabrielczar.desafiogreenmile.models.AccountCredentialsModel;
import com.gabrielczar.desafiogreenmile.models.EventModel;
import com.gabrielczar.desafiogreenmile.models.OrganizerModel;
import com.gabrielczar.desafiogreenmile.repositories.EventRepository;
import com.gabrielczar.desafiogreenmile.repositories.OrganizerRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;

import static com.gabrielczar.desafiogreenmile.enumerations.Authority.ORGANIZADOR;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
public class EventControllerTest {
    private final String EVENTS = "/api/events";

    private MockMvc mvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private OrganizerRepository organizerRepository;

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        initEvents();
    }

    @Test
    @WithAnonymousUser
    public void accessEventsUnauthorized() throws Exception {
        this.mvc.perform(get(EVENTS)
                    .accept(MediaType.APPLICATION_JSON))
            .andExpect(status()
                    .is4xxClientError());
    }

    @Test
    @WithMockUser(authorities = "PARTICIPANTE")
    public void accessEvents() throws Exception {
        this.mvc.perform(get(EVENTS)
                    .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status()
                    .is2xxSuccessful());
    }

    private void initEvents() {

        eventRepository.deleteAll();
        organizerRepository.deleteAll();

        EventModel event1 = new EventModel(),
                event2 = new EventModel();

        event1.setLocal("Quixada");
        event2.setLocal("Quixada");
        event1.setDate(LocalDate.now());
        event2.setDate(LocalDate.now().plusMonths(2));
        event1.setName("Mobilidade");
        event2.setName("Intenacionalizacao");
        event1.setNumParticipantsByTeam(4);
        event2.setNumParticipantsByTeam(5);
        event1.setNumTeams(10);
        event2.setNumTeams(10);
        event1.setDescription("Trajetorias de caminhoes");
        event2.setDescription("Internacionalizacao de alguma coisa");


        AccountCredentialsModel acc = new AccountCredentialsModel();
        acc.setUsername("organizador");
        acc.setPassword("organizador");
        acc.setAuthority(ORGANIZADOR);

        OrganizerModel organizer = new OrganizerModel();
        organizer.setNome("Organizador");
        organizer.setEmail("organizador@mail.com");
        organizer.setAccount(acc);

        organizer = organizerRepository.save(organizer);

        event1.setOrganizer(organizer);
        event2.setOrganizer(organizer);

        eventRepository.save(event1);
        eventRepository.save(event2);

    }
}
