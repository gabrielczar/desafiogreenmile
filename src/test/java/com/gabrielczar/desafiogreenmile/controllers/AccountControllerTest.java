package com.gabrielczar.desafiogreenmile.controllers;

import com.gabrielczar.desafiogreenmile.enumerations.Authority;
import com.gabrielczar.desafiogreenmile.models.AccountCredentialsModel;
import com.gabrielczar.desafiogreenmile.repositories.AccountRepository;
import com.gabrielczar.desafiogreenmile.utils.UserUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AccountControllerTest {
    private UserUtils userUtils;
    private MockMvc mvc;

    String username = "username",
            password = "password";

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private AccountRepository accountRepository;

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
        userUtils = new UserUtils(mvc);

        this.clearData();
    }

    private void clearData() {
        AccountCredentialsModel acc = accountRepository.findByUsername(username);
        if (acc != null)
            accountRepository.delete(acc);
    }

    @Test
    public void registerAccountMissAuthority() throws Exception {
        this.userUtils.registerUser(username, password, null)
                .andExpect(status().is4xxClientError())
                .andReturn();
    }

    @Test
    public void registerAccountOrganizerUnauthorized() throws Exception {
        this.userUtils.registerUser(username, password, Authority.ORGANIZADOR)
                .andExpect(status().isBadRequest());
    }

    @Test
    public void registerAccountParticipant() throws Exception {
        this.userUtils.registerUser(username, password, Authority.PARTICIPANTE)
                .andExpect(status().isCreated());
    }

    @Test
    public void registerSameAccount() throws Exception {
        this.clearData();

        this.userUtils.registerUser(username, password, Authority.PARTICIPANTE)
                .andExpect(status().is2xxSuccessful());
        this.userUtils.registerUser(username, password, Authority.PARTICIPANTE)
                .andExpect(status().is4xxClientError());
    }

}
