package com.gabrielczar.desafiogreenmile.controllers;

import com.gabrielczar.desafiogreenmile.enumerations.ShirtSize;
import com.gabrielczar.desafiogreenmile.models.AccountCredentialsModel;
import com.gabrielczar.desafiogreenmile.models.ParticipantModel;
import com.gabrielczar.desafiogreenmile.models.TeamModel;
import com.gabrielczar.desafiogreenmile.repositories.ParticipantRepository;
import com.gabrielczar.desafiogreenmile.repositories.TeamRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import static com.gabrielczar.desafiogreenmile.enumerations.Authority.PARTICIPANTE;
import static org.junit.Assert.*;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@RunWith(SpringRunner.class)
public class TeamControllerTest {
    private final String TEAMS = "/api/teams";

    private MockMvc mvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private ParticipantRepository participantRepository;

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();

        initTeamMember();
    }

    @Test
    @WithAnonymousUser
    public void listUnauthorized() throws Exception {
        this.mvc.perform(get("/api/teams"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    @WithMockUser(authorities = "PARTICIPANTE")
    public void list() throws Exception {
        this.mvc.perform(get(TEAMS)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content").exists())
                .andExpect(jsonPath("$.size").value(10))
                .andDo(mvcResult -> mvcResult.getResponse().getStatus())
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    @WithAnonymousUser
    public void listParticipant() throws Exception {
        this.mvc.perform(get("/api/teams"))
                .andExpect(status().is4xxClientError());
    }


    @Test
    @WithMockUser(authorities = "PARTICIPANTE")
    public void listParticipantAuth() throws Exception {
        this.mvc.perform(get("/api/teams"))
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content[0].members").isArray())
                .andDo(mvcResult -> mvcResult.getResponse().getStatus())
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void addTeamUnauthenticated() throws Exception {
        this.mvc.perform(post("/api/teams"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void addParticipantToTeamUnauthenticated() throws Exception {
        this.mvc.perform(post("/{tid}/participant/{pid}", 1, 1))
                .andExpect(status().is4xxClientError());
    }

    private void initTeamMember() {
        teamRepository.deleteAll();
        participantRepository.deleteAll();

        for (int i = 0; i < 10; i++) {
            AccountCredentialsModel accountCredentialsModel = new AccountCredentialsModel();
            ParticipantModel participantModel = new ParticipantModel();

            accountCredentialsModel.setAuthority(PARTICIPANTE);
            accountCredentialsModel.setUsername("part" + i);
            accountCredentialsModel.setPassword("participante");

            participantModel.setAccount(accountCredentialsModel);
            participantModel.setEmail("participant@part" + i);
            participantModel.setFullName("Participant" + i);
            participantModel.setPhone("(88) 1234-567" + i);
            participantModel.setShirtSize(ShirtSize.random());

            participantRepository.save(participantModel);

            TeamModel team = new TeamModel();
            team.setName("Tst" + i);
            team.setMembers(Collections.singleton(participantModel));
            teamRepository.saveAndFlush(team);
        }

    }
}