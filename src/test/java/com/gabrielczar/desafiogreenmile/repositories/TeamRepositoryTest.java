package com.gabrielczar.desafiogreenmile.repositories;

import com.gabrielczar.desafiogreenmile.enumerations.ShirtSize;
import com.gabrielczar.desafiogreenmile.models.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;

import static com.gabrielczar.desafiogreenmile.enumerations.Authority.ORGANIZADOR;
import static com.gabrielczar.desafiogreenmile.enumerations.Authority.PARTICIPANTE;
import static org.junit.Assert.assertNotNull;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TeamRepositoryTest {

    @Autowired
    private ParticipantRepository participantRepository;

    @Autowired
    private OrganizerRepository organizerRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private TeamRepository teamRepository;

    private TeamModel team1;

    private EventModel event;

    @Before
    public void beforeEachTest() {

        eventRepository.deleteAll();
        teamRepository.deleteAll();
        participantRepository.deleteAll();
        organizerRepository.deleteAll();

        testEvent();
    }

    @Test
    public void findAllByEventAndDate() {
        assertNotNull(this.teamRepository
                .findAllByEvent_IdAndEnrollDateBetween(
                        event.getId(),
                        team1.getEnrollDate().toLocalDate().atStartOfDay(),
                        team1.getEnrollDate().toLocalDate().plusDays(1).atStartOfDay(),
                        PageRequest.of(0, 2)));
    }

    private void testEvent()
    {
        event = new EventModel();
        event.setLocal("Quixada");

        event.setDate(LocalDate.now().plusMonths(2));
        event.setName("Hasckaton");
        event.setNumParticipantsByTeam(4);
        event.setNumTeams(10);
        event.setDescription("Internacionalizacao de algo");

        AccountCredentialsModel accountCredentialsModel = new AccountCredentialsModel();
        OrganizerModel organizerModel = new OrganizerModel();

        accountCredentialsModel.setAuthority(ORGANIZADOR);
        accountCredentialsModel.setUsername("organizer");
        accountCredentialsModel.setPassword("organizer");

        organizerModel.setAccount(accountCredentialsModel);
        organizerModel.setEmail("organizer@organizer");
        organizerModel.setNome("Organizer");
        organizerModel.setPhone("(88) 1234-567");

        organizerModel = organizerRepository.save(organizerModel);
        event.setOrganizer(organizerModel);

        event = eventRepository.save(event);

        AccountCredentialsModel model = new AccountCredentialsModel();
        ParticipantModel part = new ParticipantModel();

        model.setAuthority(PARTICIPANTE);
        model.setUsername("participant");
        model.setPassword("participant");

        part.setAccount(model);
        part.setFullName("Participant full");
        part.setEmail("participant@mail.com");
        part.setShirtSize(ShirtSize.random());

        participantRepository.save(part);

        team1 = new TeamModel();
        team1.setName("Team");
        team1.setEnrollDate(LocalDateTime.now());
        team1.setEvent(event);
        team1.setMembers(Collections.singleton(part));

        teamRepository.save(team1);

        TeamModel team2 = new TeamModel();
        team2.setName("Team Model");
        team2.setEvent(event);
        team2.setEnrollDate(LocalDateTime.now());
        team2.setMembers(Collections.singleton(part));

        teamRepository.save(team2);
    }

}