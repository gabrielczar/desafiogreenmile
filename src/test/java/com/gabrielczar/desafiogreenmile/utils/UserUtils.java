package com.gabrielczar.desafiogreenmile.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gabrielczar.desafiogreenmile.enumerations.Authority;
import com.gabrielczar.desafiogreenmile.models.AccountWrapper;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.io.IOException;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class UserUtils {
    private MockMvc mockMvc;

    private ObjectMapper mapper;

    public UserUtils(MockMvc mockMvc) {
        this.mockMvc = mockMvc;
        mapper = new ObjectMapper();
    }

    public String json(Object o) throws IOException {
        return mapper.writeValueAsString(o);
    }

    public ResultActions registerUser(String username, String password, Authority auth) throws Exception {
        AccountWrapper acc = new AccountWrapper();
        acc.setUsername(username);
        acc.setPassword(password);
        acc.setAuthority(auth);

        return mockMvc.perform(
                post("/api/public/account")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json(acc)));
    }
}
