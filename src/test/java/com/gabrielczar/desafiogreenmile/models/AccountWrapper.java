package com.gabrielczar.desafiogreenmile.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountWrapper {
    private String username;
    private String password;
    private com.gabrielczar.desafiogreenmile.enumerations.Authority Authority;
}
