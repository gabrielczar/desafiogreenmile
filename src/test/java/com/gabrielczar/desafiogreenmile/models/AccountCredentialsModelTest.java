package com.gabrielczar.desafiogreenmile.models;

import com.gabrielczar.desafiogreenmile.enumerations.Authority;
import org.junit.Test;

import static org.junit.Assert.*;

public class AccountCredentialsModelTest {

    private final Integer ID = 1;
    private final String USERNAME = "username";
    private final String PASSWORD = "password";

    @Test
    public void callingAccountConstructorWithoutParameters() {
        AccountCredentialsModel user = new AccountCredentialsModel();


        assertNull(user.getId());
        assertNull(user.getUsername());
        assertNull(user.getPassword());
        assertNull(user.getAuthority());

        assertTrue(user.getEnable());
    }

    @Test
    public void callingAccountConstructorWithParameters() {
        AccountCredentialsModel account = new AccountCredentialsModel
                (ID, USERNAME, PASSWORD, Authority.ORGANIZADOR, true);

        assertEquals("Id equals", account.getId(), ID);
        assertEquals("Username equals", account.getUsername(), USERNAME);
        assertEquals("Password equals", account.getPassword(), PASSWORD);
        assertEquals("Authority equals", account.getAuthority(), Authority.ORGANIZADOR);

        assertTrue(account.getEnable());

    }

}