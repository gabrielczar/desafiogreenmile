package com.gabrielczar.desafiogreenmile.services;

import com.gabrielczar.desafiogreenmile.models.AccountCredentialsModel;
import com.gabrielczar.desafiogreenmile.repositories.AccountRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.junit4.SpringRunner;

import static com.gabrielczar.desafiogreenmile.enumerations.Authority.PARTICIPANTE;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ImplUserDetailsServiceTest {
    private final String USER_EXIST = "organizer";
    private final String USER_NOT_EXIST = "not_exists";

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private UserDetailsService userDetailsService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        initUser();
    }

    private void initUser() {
        AccountCredentialsModel model = accountRepository.findByUsername(USER_EXIST);
        if (model == null) {
            AccountCredentialsModel acc = new AccountCredentialsModel();
            acc.setUsername(USER_EXIST);
            acc.setPassword(USER_EXIST);
            acc.setAuthority(PARTICIPANTE);

            accountRepository.save(acc);
        }
    }

    @Test
    public void loadUserByUsername() {
        assertNull("Null username", userDetailsService.loadUserByUsername(null));
        assertNull("Doesn't exist username", userDetailsService.loadUserByUsername(USER_NOT_EXIST));

        assertNotNull("Exist user", userDetailsService.loadUserByUsername(USER_EXIST));
    }


}