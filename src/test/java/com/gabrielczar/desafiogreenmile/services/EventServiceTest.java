package com.gabrielczar.desafiogreenmile.services;

import com.gabrielczar.desafiogreenmile.enumerations.ShirtSize;
import com.gabrielczar.desafiogreenmile.models.*;
import com.gabrielczar.desafiogreenmile.repositories.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;

import static com.gabrielczar.desafiogreenmile.enumerations.Authority.ORGANIZADOR;
import static com.gabrielczar.desafiogreenmile.enumerations.Authority.PARTICIPANTE;
import static org.junit.Assert.fail;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
public class EventServiceTest {
    private final String URL_EVENT = "/api/events",
            URL_EVENT_CLOSE = "/{id}/close",
            URL_EVENT_TEAM = "/{eid}/team/{tid}";

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private OrganizerRepository organizerRepository;

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private ParticipantRepository participantRepository;

    private EventModel event;

    private TeamModel team;

    private ParticipantModel participant;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(springSecurity()).build();

        if (teamRepository.count() > 0)
            teamRepository.deleteAll();
        if (eventRepository.count() > 0)
            eventRepository.deleteAll();
        if (organizerRepository.count() > 0)
            organizerRepository.deleteAll();
        if (participantRepository.count() > 0)
            participantRepository.deleteAll();
    }

    private void initParticipant(String username) {
        AccountCredentialsModel acc = new AccountCredentialsModel();
        acc.setUsername(username);
        acc.setPassword(username);
        acc.setAuthority(PARTICIPANTE);

        participant = new ParticipantModel();
        participant.setShirtSize(ShirtSize.random());
        participant.setFullName(username + " full name");
        participant.setEmail(username + "@mail.com");
        participant.setAccount(acc);

        participant = participantRepository.save(participant);
    }

    public void initEvent(String username) {

        AccountCredentialsModel acc = new AccountCredentialsModel();
        acc.setUsername(username);
        acc.setPassword(username);
        acc.setAuthority(ORGANIZADOR);

        OrganizerModel organizer = new OrganizerModel();
        organizer.setNome("Organizador");
        organizer.setEmail(username + "@mail.com");
        organizer.setAccount(acc);

        organizer = organizerRepository.save(organizer);

        event = new EventModel();
        event.setOrganizer(organizer);
        event.setNumTeams(1);
        event.setDescription("Teste Service");
        event.setName("TestService");
        event.setNumParticipantsByTeam(1);
        event.setLocal("Quixada");
        event.setDate(LocalDate.now());

        event = eventRepository.save(event);
    }

    private void initTeam(String name) {
        team = new TeamModel();
        team.setName(name);
        team.setEnrollDate(LocalDateTime.now());
        team = teamRepository.save(team);
    }

    @Test
    @WithMockUser(authorities = "ORGANIZADOR")
    public void closeEventAuthorized() throws Exception {
        initEvent("organizador");
        this.mockMvc.perform(put(URL_EVENT + URL_EVENT_CLOSE, event.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    @WithMockUser(authorities = "PARTICIPANTE")
    public void closeEventUnauthorized() throws Exception {
        initEvent("participante");
        this.mockMvc.perform(put(URL_EVENT + URL_EVENT_CLOSE, event.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    @WithMockUser(authorities = "PARTICIPANTE")
    public void addTeam() throws Exception {
        initTeam("Null");
        initEvent("add_teams");
        addTeamRequest();
    }

    @Test
    @WithMockUser(authorities = "PARTICIPANTE")
    public void addTeamNameEqual() throws Exception {
        addTeam();

        this.mockMvc
                .perform(post(URL_EVENT + URL_EVENT_TEAM,
                        event.getId(), team.getId())
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    private void addTeamRequest() throws Exception {
        this.mockMvc
                .perform(post(URL_EVENT + URL_EVENT_TEAM,
                        event.getId(), team.getId())
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    @WithMockUser(authorities = "PARTICIPANTE")
    public void addTeamLimitWithEventOpen() throws Exception { // limit 2 teams
        addTeam();

        initTeam("New");

        addTeamRequest();

        initTeam("Limit");

        addTeamRequest();
    }

    @Test
    @WithMockUser(authorities = {"PARTICIPANTE", "ORGANIZADOR"})
    public void addTeamLimitWithEventClose() throws Exception {
        addTeam();

        initTeam("New");

        addTeamRequest();

        initTeam("Limit");

        closeEventAuthorized();

        this.mockMvc
                .perform(post(URL_EVENT + URL_EVENT_TEAM,
                        event.getId(), team.getId())
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    @WithMockUser(authorities = "PARTICIPANTE")
    public void addTeamWithSameParticipant() throws Exception {
        EventModel eventModel1 = new EventModel();
        eventModel1.setLocal("Quixada");

        eventModel1.setDate(LocalDate.now().plusMonths(2));
        eventModel1.setName("Mobilidade");
        eventModel1.setNumParticipantsByTeam(4);
        eventModel1.setNumTeams(10);
        eventModel1.setDescription("Internacionalizacao de alguma coisa");

        AccountCredentialsModel accountCredentialsModel1 = new AccountCredentialsModel();
        OrganizerModel organizerModel1 = new OrganizerModel();

        accountCredentialsModel1.setAuthority(ORGANIZADOR);
        accountCredentialsModel1.setUsername("organizer");
        accountCredentialsModel1.setPassword("organizer");

        organizerModel1.setAccount(accountCredentialsModel1);
        organizerModel1.setEmail("organizer@organizer");
        organizerModel1.setNome("Organizer");
        organizerModel1.setPhone("(88) 1234-567");

        organizerModel1 = organizerRepository.save(organizerModel1);
        eventModel1.setOrganizer(organizerModel1);

        eventModel1 = eventRepository.save(eventModel1);

        AccountCredentialsModel model1 = new AccountCredentialsModel();
        ParticipantModel part1 = new ParticipantModel();

        model1.setAuthority(PARTICIPANTE);
        model1.setUsername("participant");
        model1.setPassword("participant");

        part1.setAccount(model1);
        part1.setFullName("Participant full");
        part1.setEmail("participant@mail.com");
        part1.setShirtSize(ShirtSize.random());

        participantRepository.save(part1);

        TeamModel team1 = new TeamModel();
        team1.setName("Team");
        team1.setEnrollDate(LocalDateTime.now());
        team1.setEvent(eventModel1);
        team1.setMembers(Collections.singleton(part1));

        teamRepository.save(team1);

        TeamModel teamModel1 = new TeamModel();
        teamModel1.setName("Team Model");
        teamModel1.setMembers(Collections.singleton(part1));

        teamRepository.save(teamModel1);

        this.mockMvc
                .perform(post(URL_EVENT + URL_EVENT_TEAM,
                        eventModel1.getId(), teamModel1.getId())
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    @WithMockUser(authorities = "PARTICIPANTE")
    public void addTeamWithParticipantLimit() throws Exception { // 2 participants in team
        initEvent("new_teams");
        initTeam("New");
        initParticipant("participant1");

        team.setMembers(Collections.singleton(participant));
        participant.setTeams(Collections.singletonList(team));
        team = teamRepository.saveAndFlush(team);

        initParticipant("participant2");

        team.getMembers().add(participant);
        participant.setTeams(Collections.singletonList(team));
        teamRepository.saveAndFlush(team);

        this.mockMvc
                .perform(post(URL_EVENT + URL_EVENT_TEAM,
                        event.getId(), team.getId())
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    @WithMockUser(authorities = "PARTICIPANTE")
    public void remTeam() throws Exception {
        addTeam();
        this.mockMvc
                .perform(delete(URL_EVENT + URL_EVENT_TEAM, event.getId(), team.getId())
                    .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is2xxSuccessful());
    }
}