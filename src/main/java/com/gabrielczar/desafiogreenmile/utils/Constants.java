package com.gabrielczar.desafiogreenmile.utils;

public class Constants {
    // 10 days
    public static final long
            EXPIRATION_TIME = 860_000_000;

    public static final String
            SECRET = "API_CHALLENGE_SECRET",
            TOKEN_PREFIX = "Bearer ",
            HEADER_STRING = "Authorization";

    public static final String URL_LOGIN = "/api/public/login";
}
