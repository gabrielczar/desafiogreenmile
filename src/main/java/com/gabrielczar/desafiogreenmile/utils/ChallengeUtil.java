package com.gabrielczar.desafiogreenmile.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class ChallengeUtil {

    public static LocalDate datefromString(String value) {
        final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd", new Locale("pt", "BR"));
        return LocalDate.parse(value, dtf);
    }

}
