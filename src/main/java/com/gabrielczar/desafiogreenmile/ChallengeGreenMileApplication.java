package com.gabrielczar.desafiogreenmile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChallengeGreenMileApplication {
    public static void main(String[] args) {
        SpringApplication.run(ChallengeGreenMileApplication.class, args);
    }
}
