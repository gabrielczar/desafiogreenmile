package com.gabrielczar.desafiogreenmile.bootstrap;


import com.gabrielczar.desafiogreenmile.enumerations.ShirtSize;
import com.gabrielczar.desafiogreenmile.models.*;
import com.gabrielczar.desafiogreenmile.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;

import static com.gabrielczar.desafiogreenmile.enumerations.Authority.ORGANIZADOR;
import static com.gabrielczar.desafiogreenmile.enumerations.Authority.PARTICIPANTE;
import static org.springframework.data.domain.PageRequest.of;

@Component
@Profile("dev")
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {
    private final Logger logger = Logger.getLogger(this.getClass().getName());

    private AccountRepository accountRepository;
    private OrganizerRepository organizerRepository;
    private ParticipantRepository participantRepository;
    private EventRepository eventRepository;
    private TeamRepository teamRepository;

    @Autowired
    public DevBootstrap(AccountRepository accountRepository, OrganizerRepository organizerRepository, ParticipantRepository participantRepository, EventRepository eventRepository, TeamRepository teamRepository) {
        this.accountRepository = accountRepository;
        this.organizerRepository = organizerRepository;
        this.participantRepository = participantRepository;
        this.eventRepository = eventRepository;
        this.teamRepository = teamRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        // insert initial data
//
//        if (accountRepository.count() > 0L)
//            return;
//
//        initOrganizers();
//
//        initParticipants();
//
//        initEvents();
//
//        initTeams();
//
//        initTeamMember();

//        testEvent();
    }

    private void testEvent()
    {
        EventModel eventModel = new EventModel();
        eventModel.setLocal("Quixada");

        eventModel.setDate(LocalDate.now().plusMonths(2));
        eventModel.setName("Mobilidade");
        eventModel.setNumParticipantsByTeam(4);
        eventModel.setNumTeams(10);
        eventModel.setDescription("Internacionalizacao de alguma coisa");

        AccountCredentialsModel accountCredentialsModel = new AccountCredentialsModel();
        OrganizerModel organizerModel = new OrganizerModel();

        accountCredentialsModel.setAuthority(ORGANIZADOR);
        accountCredentialsModel.setUsername("organizer");
        accountCredentialsModel.setPassword("organizer");

        organizerModel.setAccount(accountCredentialsModel);
        organizerModel.setEmail("organizer@organizer");
        organizerModel.setNome("Organizer");
        organizerModel.setPhone("(88) 1234-567");

        organizerModel = organizerRepository.save(organizerModel);
        eventModel.setOrganizer(organizerModel);

        eventModel = eventRepository.save(eventModel);

        AccountCredentialsModel model = new AccountCredentialsModel();
        ParticipantModel part = new ParticipantModel();

        model.setAuthority(PARTICIPANTE);
        model.setUsername("participant");
        model.setPassword("participant");

        part.setAccount(model);
        part.setFullName("Participant full");
        part.setEmail("participant@mail.com");
        part.setShirtSize(ShirtSize.random());

        participantRepository.save(part);

        TeamModel team = new TeamModel();
        team.setName("Team");
        team.setEnrollDate(LocalDateTime.now());
        team.setEvent(eventModel);
        team.setMembers(Collections.singleton(part));

        teamRepository.save(team);

        TeamModel teamModel = new TeamModel();
        teamModel.setName("Team Model");
        teamModel.setEvent(eventModel);
        teamModel.setEnrollDate(LocalDateTime.now());
        teamModel.setMembers(Collections.singleton(part));

        teamRepository.save(teamModel);
    }

    private void initTeams() {
        List<TeamModel> teams = new ArrayList<>();
        List<ParticipantModel> parts = participantRepository.findAll();


        for (int i = 0; i < 20; i += 2) {
            TeamModel team = new TeamModel();
            team.setName("Teste");
            team.setMembers(new HashSet<>(parts.subList(i, i + 2)));
            teams.add(team);
        }

        teamRepository.saveAll(teams);

    }

    private void initTeamMember() {

//        List<TeamModel> teams = new ArrayList<>();
        List<ParticipantModel> lst = new ArrayList<>();

        for (int i = 0; i < 20; i++) {
            AccountCredentialsModel accountCredentialsModel = new AccountCredentialsModel();
            ParticipantModel participantModel = new ParticipantModel();

            accountCredentialsModel.setAuthority(PARTICIPANTE);
            accountCredentialsModel.setUsername("part" + i);
            accountCredentialsModel.setPassword("participante");

            participantModel.setAccount(accountCredentialsModel);
            participantModel.setEmail("participant@part" + i);
            participantModel.setFullName("Participant" + i);
            participantModel.setPhone("(88) 1234-567" + i);
            participantModel.setShirtSize(ShirtSize.random());

            lst.add(participantModel);

            if (i % 2 == 0) {
                participantRepository.saveAll(lst);
                TeamModel team = new TeamModel();
                team.setName("Tst" + i);
                team.setMembers(new HashSet<>(lst));
//                teams.add(team);
                teamRepository.saveAndFlush(team);
                lst = new ArrayList<>();
            }
        }

//        teamRepository.saveAll(teams);
    }


    private void initOrganizers() {
        List<OrganizerModel> lst = new ArrayList<>();

        for (int i = 0; i < 6; i++) {
            AccountCredentialsModel accountCredentialsModel = new AccountCredentialsModel();
            OrganizerModel organizerModel = new OrganizerModel();

            accountCredentialsModel.setAuthority(ORGANIZADOR);
            accountCredentialsModel.setUsername("organizer" + i);
            accountCredentialsModel.setPassword("organizer");

            organizerModel.setAccount(accountCredentialsModel);
            organizerModel.setEmail("organizer@organizer" + i);
            organizerModel.setNome("Organizer" + i);
            organizerModel.setPhone("(88) 1234-567" + i);

            lst.add(organizerModel);
        }

        organizerRepository.saveAll(lst);
    }

    private void initParticipants() {
        List<ParticipantModel> lst = new ArrayList<>();

        for (int i = 0; i < 20; i++) {
            AccountCredentialsModel accountCredentialsModel = new AccountCredentialsModel();
            ParticipantModel participantModel = new ParticipantModel();

            accountCredentialsModel.setAuthority(PARTICIPANTE);
            accountCredentialsModel.setUsername("participant" + i);
            accountCredentialsModel.setPassword("participant");


            participantModel.setAccount(accountCredentialsModel);
            participantModel.setEmail("participant@participant" + i);
            participantModel.setFullName("Participant" + i);
            participantModel.setPhone("(88) 1234-567" + i);
            participantModel.setShirtSize(ShirtSize.random());

            lst.add(participantModel);
        }

        participantRepository.saveAll(lst);
    }


    private void initEvents() {

        EventModel event1 = new EventModel(),
                event2 = new EventModel();

        event1.setLocal("Quixada");
        event2.setLocal("Quixada");
        event1.setDate(LocalDate.now().plusMonths(2));
        event2.setDate(LocalDate.now().plusMonths(3));
        event1.setName("Mobilidade");
        event2.setName("Intenacionalizacao");
        event1.setNumParticipantsByTeam(4);
        event2.setNumParticipantsByTeam(5);
        event1.setNumTeams(10);
        event2.setNumTeams(10);
        event1.setDescription("Descoberta de trajetorias de caminhoes");
        event2.setDescription("Internacionalizacao de alguma coisa");

        List<OrganizerModel> organizer = (List<OrganizerModel>) organizerRepository.findAll();
        OrganizerModel organizer1 = organizer.get(0);
        OrganizerModel organizer2 = organizer.get(1);

        event1.setOrganizer(organizer1);
        event2.setOrganizer(organizer2);

        eventRepository.save(event1);
        eventRepository.save(event2);

    }

}
