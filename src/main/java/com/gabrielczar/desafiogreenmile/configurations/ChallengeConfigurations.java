package com.gabrielczar.desafiogreenmile.configurations;

import com.gabrielczar.desafiogreenmile.services.StorageService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ChallengeConfigurations {

    @Bean
    StorageService storageService() {
        return new StorageService("challenge");
    }
}
