package com.gabrielczar.desafiogreenmile.configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.Filter;

import static com.gabrielczar.desafiogreenmile.utils.Constants.URL_LOGIN;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final UserDetailsService userDetailsService;
    private final RestAuthenticationEntryPoint restAuthenticationEntryPoint;

    @Autowired
    public SecurityConfig(UserDetailsService userDetailsService
            , RestAuthenticationEntryPoint restAuthenticationEntryPoint
    ){
        this.userDetailsService = userDetailsService;
        this.restAuthenticationEntryPoint = restAuthenticationEntryPoint;
    }

    @Override
    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @Bean
    Filter authenticationFilter() {
        return new JwtAuthenticationTokenFilter(jwtTokenCodec());
    }

    @Bean
    JwtTokenCodec jwtTokenCodec() {
        return new JwtTokenCodec();
    }

    @Override
	protected void configure(HttpSecurity http) throws Exception {
		http
                .csrf()
                    .disable()
                .headers()
                    .cacheControl()
                    .and()
                    .and()
                .exceptionHandling()
                    .authenticationEntryPoint(restAuthenticationEntryPoint)
                    .and()
                .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and()
                .authorizeRequests()
                    .antMatchers("/h2/**", "/api/public/**")
                    .permitAll()
                .antMatchers(HttpMethod.POST, URL_LOGIN)
                    .permitAll()
                .anyRequest()
				    .authenticated()
                .and()
                    .addFilterBefore(authenticationFilter() ,
                            UsernamePasswordAuthenticationFilter.class);
	}

    @Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(userDetailsService)
                .passwordEncoder(new BCryptPasswordEncoder());

//		auth.inMemoryAuthentication()
//                .passwordEncoder(new BCryptPasswordEncoder())
//				.withUser("admin")
//				.password(new BCryptPasswordEncoder().encode("admin"))
//                .roles(ORGANIZADOR.getAuthority());
	}

}