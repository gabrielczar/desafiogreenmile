package com.gabrielczar.desafiogreenmile.configurations;


import com.gabrielczar.desafiogreenmile.enumerations.Authority;
import com.gabrielczar.desafiogreenmile.utils.Constants;
import com.gabrielczar.desafiogreenmile.wrappers.JwtTokenWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Collections;

import static com.gabrielczar.desafiogreenmile.utils.Constants.TOKEN_PREFIX;

@Component
public class JwtAuthenticationTokenFilter extends GenericFilterBean {
    private JwtTokenCodec jwtTokenCodec;

    @Autowired
    public JwtAuthenticationTokenFilter(JwtTokenCodec jwtTokenCodec) {
        this.jwtTokenCodec = jwtTokenCodec;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        final HttpServletRequest httpRequest = (HttpServletRequest) request;
        final String header = httpRequest.getHeader(Constants.HEADER_STRING);
        final SecurityContext context = SecurityContextHolder.getContext();
        if (header != null && context.getAuthentication() == null) {
            final String tokenStr = header.substring(TOKEN_PREFIX.length());
            final JwtTokenWrapper token = jwtTokenCodec.decodeToken(tokenStr);
            if (!token.isExpired()) {
                final PreAuthenticatedAuthenticationToken authentication =
                        new PreAuthenticatedAuthenticationToken(token, "n/a", Collections.singletonList(Authority.valueOf(token.getRole())));
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpRequest));
                context.setAuthentication(authentication);
            }
        }
        chain.doFilter(request, response);
    }
}