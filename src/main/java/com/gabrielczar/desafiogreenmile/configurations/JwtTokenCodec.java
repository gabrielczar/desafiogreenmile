package com.gabrielczar.desafiogreenmile.configurations;

import com.gabrielczar.desafiogreenmile.utils.Constants;
import com.gabrielczar.desafiogreenmile.wrappers.JwtTokenWrapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

import java.util.Date;

import static com.gabrielczar.desafiogreenmile.utils.Constants.EXPIRATION_TIME;
import static com.gabrielczar.desafiogreenmile.utils.Constants.SECRET;

@Component
public class JwtTokenCodec {

    public JwtTokenWrapper decodeToken(String token) {
        return new JwtTokenWrapper(Jwts.parser()
                .setSigningKey(Constants.SECRET)
                .parseClaimsJws(token)
                .getBody());
    }

    private Date generateExpirationDate(Date issuedAt) {
        return new Date(issuedAt.getTime() + EXPIRATION_TIME);
    }

    public String encodeToken(JwtTokenWrapper token) {
        return Jwts.builder()
                .setClaims(token.getClaims())
                .setIssuedAt(new Date())
                .setExpiration(generateExpirationDate(new Date()))
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
    }
}
