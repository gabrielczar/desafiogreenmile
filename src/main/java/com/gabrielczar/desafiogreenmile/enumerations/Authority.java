package com.gabrielczar.desafiogreenmile.enumerations;

import org.springframework.security.core.GrantedAuthority;

public enum Authority implements GrantedAuthority {
    ORGANIZADOR("ORGANIZADOR"), PARTICIPANTE("PARTICIPANTE");

    private String name;

	Authority(String name) {
	    this.name = name;
    }

    @Override
    public String getAuthority() {
        return this.name;
    }


}
