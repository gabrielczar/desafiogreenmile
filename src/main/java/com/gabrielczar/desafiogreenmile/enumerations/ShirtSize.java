package com.gabrielczar.desafiogreenmile.enumerations;

import lombok.Getter;

import java.util.Random;

@Getter
public enum ShirtSize {
    PP("PP"), P("P"), M("M"), G("G"), GG("GG"), EGG("EGG");

    private String value;

    ShirtSize(String value) {
        this.value = value;
    }

    public static ShirtSize random() {
        int size = ShirtSize.values().length;
        int pos = new Random().nextInt(size);

        return ShirtSize.values()[pos % size];
    }
}
