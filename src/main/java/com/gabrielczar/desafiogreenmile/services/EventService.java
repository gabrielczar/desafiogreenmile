package com.gabrielczar.desafiogreenmile.services;

import com.gabrielczar.desafiogreenmile.models.EventModel;
import com.gabrielczar.desafiogreenmile.models.ParticipantModel;
import com.gabrielczar.desafiogreenmile.models.TeamModel;
import com.gabrielczar.desafiogreenmile.repositories.EventRepository;
import com.gabrielczar.desafiogreenmile.repositories.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class EventService {
//    private final Logger logger = Logger.getLogger(this.getClass().getName());

    private final EventRepository eventRepository;
    private final TeamRepository teamRepository;

    @Autowired
    public EventService(EventRepository eventRepository, TeamRepository teamRepository) {
        this.eventRepository = eventRepository;
        this.teamRepository = teamRepository;
    }

    public void closeEvent(Integer eventID) {
        Optional<EventModel> event = eventRepository.findById(eventID);

        event.ifPresent(eventModel -> {
            eventModel.closeEvent();
            eventRepository.save(eventModel);
        });
    }

    public boolean addTeam(Integer eventId, Integer teamId) {
        EventModel event = eventRepository.getOne(eventId);
        TeamModel team = teamRepository.getOne(teamId);

        if (event == null || team == null)
            return false;

        if (event.getOpen()) {
            // max members by team
            if (team.getMembers().size() > event.getNumParticipantsByTeam())
                return false;
            // Team has the same name in event
            if (event.getTeams().contains(team))
                return false;

            for (TeamModel teamInEvent : event.getTeams()){
                // has someone in other team
                for (ParticipantModel ptTeamToAdd : team.getMembers()) {
                    if (teamInEvent.getMembers().contains(ptTeamToAdd))
                        return false;
                }
            }

            team.setEnrollDate(LocalDateTime.now());
            team.setEvent(event);
            event.getTeams().add(team);
            eventRepository.save(event);
            return true;
        }
        return false;
    }

    public void remTeam(Integer eventId, Integer teamId) {
        Optional<EventModel> event = eventRepository.findById(eventId);
        Optional<TeamModel> team = teamRepository.findById(teamId);

        event.ifPresent(eventModel ->
                team.ifPresent(teamModel -> {
                    eventModel.getTeams().remove(teamModel);
                    eventRepository.save(eventModel);
                })
        );
    }

    public EventModel save(EventModel event) {
        return eventRepository.save(event);
    }
}
