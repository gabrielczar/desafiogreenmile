package com.gabrielczar.desafiogreenmile.services;

import com.gabrielczar.desafiogreenmile.models.ParticipantModel;
import com.gabrielczar.desafiogreenmile.models.TeamModel;
import com.gabrielczar.desafiogreenmile.repositories.ParticipantRepository;
import com.gabrielczar.desafiogreenmile.repositories.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TeamService {
    private final TeamRepository teamRepository;
    private final ParticipantRepository participantRepository;

    @Autowired
    public TeamService(TeamRepository teamRepository, ParticipantRepository participantRepository) {
        this.teamRepository = teamRepository;
        this.participantRepository = participantRepository;
    }

    public void addParticipant(Integer teamId, Integer partId) {
        Optional<TeamModel> team = teamRepository.findById(teamId);
        Optional<ParticipantModel> participant = participantRepository.findById(partId);

        team.ifPresent(teamModel ->
            participant.ifPresent(participantModel -> {
                // limit with and without event
                if ((teamModel.getEvent() != null &&
                        teamModel.getMembers().size() < teamModel.getEvent().getNumParticipantsByTeam()) ||
                        teamModel.getEvent() == null) {
                    participantModel.getTeams().add(teamModel);
                    teamModel.getMembers().add(participantModel);
                    teamRepository.saveAndFlush(teamModel);
                }
            })
        );
    }
}
