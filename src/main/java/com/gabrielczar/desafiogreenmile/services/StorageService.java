package com.gabrielczar.desafiogreenmile.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

@Service
public class StorageService {
    private final Path rootLocation;

    @Autowired
    public StorageService(@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") String root) {
        this.rootLocation = init(root);
    }

    public Path init(String root) {
        if (!Files.exists(Paths.get(root), LinkOption.NOFOLLOW_LINKS)) {
            try {
                Files.createDirectories(Paths.get(root));
            } catch (IOException e) {
                e.printStackTrace();
                // return path without access
            }
        }
        return Paths.get(root);
    }

    public Path load(String filename) {
        return rootLocation.resolve(filename);
    }

    public byte[] loadAsBytes(String filename) throws IOException {
        File file = load(filename).toFile();
        InputStream stream = new FileInputStream(file);
        return FileCopyUtils.copyToByteArray(stream);
    }

    public void store(String filename, String content) throws IOException {
        byte[] data = Base64.getDecoder().decode(content.getBytes(StandardCharsets.UTF_8));
        Path destinationFile = load(filename);
        Files.write(destinationFile, data);
    }

}
