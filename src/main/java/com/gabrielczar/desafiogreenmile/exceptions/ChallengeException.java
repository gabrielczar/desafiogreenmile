package com.gabrielczar.desafiogreenmile.exceptions;

public class ChallengeException extends Exception {
    private String message;

    public ChallengeException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

}
