package com.gabrielczar.desafiogreenmile.controllers;

import com.gabrielczar.desafiogreenmile.models.AccountCredentialsModel;
import com.gabrielczar.desafiogreenmile.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static com.gabrielczar.desafiogreenmile.enumerations.Authority.ORGANIZADOR;

@RestController
@RequestMapping("/api/public/account")
public class AccountController {

//    Logger logger = Logger.getLogger(this.getClass().getName());

    private final AccountRepository accountRepository;

    @Autowired
    public AccountController(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @PostMapping
    public ResponseEntity<AccountCredentialsModel> createAccount(@Valid @RequestBody AccountCredentialsModel account) {

        if (account.getAuthority() == null)
            return ResponseEntity.badRequest().build();
        // so organizador add outro organizador
        if (account.getAuthority().getAuthority().equals(ORGANIZADOR.getAuthority()))
            return ResponseEntity.badRequest().build();

        if (accountRepository.findByUsername(account.getUsername()) != null)
            return ResponseEntity.badRequest().build();

        AccountCredentialsModel acc = accountRepository.save(account);

        return new ResponseEntity<>(acc, HttpStatus.CREATED);
    }

}
