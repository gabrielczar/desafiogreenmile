package com.gabrielczar.desafiogreenmile.controllers;

import com.gabrielczar.desafiogreenmile.models.EventModel;
import com.gabrielczar.desafiogreenmile.repositories.EventRepository;
import com.gabrielczar.desafiogreenmile.repositories.ParticipantRepository;
import com.gabrielczar.desafiogreenmile.repositories.TeamRepository;
import com.gabrielczar.desafiogreenmile.services.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

import static com.gabrielczar.desafiogreenmile.utils.ChallengeUtil.datefromString;

@RestController
@RequestMapping("/api/events")
public class EventController {
    private final EventService eventService;
    private final EventRepository eventRepository;
    private final ParticipantRepository participantRepository;
    private final TeamRepository teamRepository;

    @Autowired
    public EventController(EventService eventService, EventRepository eventRepository, ParticipantRepository participantRepository, TeamRepository teamRepository) {
        this.eventService = eventService;
        this.eventRepository = eventRepository;
        this.participantRepository = participantRepository;
        this.teamRepository = teamRepository;
    }

    @GetMapping
    public ResponseEntity<?> list(Pageable pageRequest) {
        Page<EventModel> page = eventRepository.findAll(pageRequest);
        return ResponseEntity.ok(page);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ORGANIZADOR')")
    public ResponseEntity save(@RequestBody EventModel event) {
        return ResponseEntity.ok(eventService.save(event));
    }

    @PutMapping("/{id}/close")
    @PreAuthorize("hasAuthority('ORGANIZADOR')")
    public ResponseEntity closeEvent(@PathVariable("id") Integer id) {
        eventService.closeEvent(id);
        return ResponseEntity.ok(id);
    }

    @PostMapping("/{eid}/team/{tid}")
    public ResponseEntity addTeam(@PathVariable("eid") Integer eventId, @PathVariable("tid") Integer teamId) {
        if (eventService.addTeam(eventId, teamId))
            return ResponseEntity.accepted().build();
        return ResponseEntity.badRequest().build();
    }

    @DeleteMapping("/{eid}/team/{tid}")
    public ResponseEntity remTeam(@PathVariable("eid") Integer eventId, @PathVariable("tid") Integer teamId) {
        eventService.remTeam(eventId, teamId);
        return ResponseEntity.accepted().build();
    }

    @GetMapping("/{eid}/participants/enroll-date/{date}")
    public ResponseEntity listParticipantsByEnrollDate(@PathVariable("eid")Integer eventId, @PathVariable("date") String date, Pageable pageRequest) {
        LocalDateTime initDate = datefromString(date).atStartOfDay();
        LocalDateTime endDate = datefromString(date).plusDays(1).atStartOfDay();
        return ResponseEntity.ok(participantRepository.findAllByEventAndEnrollDate(eventId, initDate, endDate, pageRequest));
    }

    @GetMapping("/{eid}/teams/enroll-date/{date}")
    public ResponseEntity listTeamByEnrollDate(@PathVariable("eid")Integer eventId, @PathVariable("date") String date, Pageable pageRequest) {
        LocalDateTime initDate = datefromString(date).atStartOfDay();
        LocalDateTime endDate = datefromString(date).plusDays(1).atStartOfDay();
        return ResponseEntity.ok(teamRepository.findAllByEvent_IdAndEnrollDateBetween(eventId, initDate, endDate, pageRequest));
    }

}
