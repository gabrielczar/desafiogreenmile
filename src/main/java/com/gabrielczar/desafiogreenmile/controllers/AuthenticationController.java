package com.gabrielczar.desafiogreenmile.controllers;

import com.gabrielczar.desafiogreenmile.configurations.JwtTokenCodec;
import com.gabrielczar.desafiogreenmile.models.AccountCredentialsModel;
import com.gabrielczar.desafiogreenmile.wrappers.AuthenticationResponseWrapper;
import com.gabrielczar.desafiogreenmile.wrappers.JwtTokenWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/public")
public class AuthenticationController {
    private final AuthenticationManager authenticationManager;
    private final JwtTokenCodec jwtTokenCodec;

    @Autowired
    public AuthenticationController(
            AuthenticationManager authenticationManager,
            JwtTokenCodec jwtTokenCodec) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenCodec = jwtTokenCodec;
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody AccountCredentialsModel authenticationRequest) throws AuthenticationException {
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        authenticationRequest.getUsername(),
                        authenticationRequest.getPassword()
                ));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final AccountCredentialsModel userDetails = (AccountCredentialsModel) authentication.getPrincipal();
        final String token = jwtTokenCodec.encodeToken(JwtTokenWrapper.ofUser(userDetails));
        return ResponseEntity.ok(new AuthenticationResponseWrapper(token, userDetails));
    }
}
