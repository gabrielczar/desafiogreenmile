package com.gabrielczar.desafiogreenmile.controllers;

import com.gabrielczar.desafiogreenmile.models.TeamModel;
import com.gabrielczar.desafiogreenmile.repositories.TeamRepository;
import com.gabrielczar.desafiogreenmile.services.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/teams")
public class TeamController {
    private final TeamRepository teamRepository;
    private final TeamService teamService;

    @Autowired
    public TeamController(TeamRepository teamRepository, TeamService teamService) {
        this.teamRepository = teamRepository;
        this.teamService = teamService;
    }

    @GetMapping
    public ResponseEntity<?> list(Pageable pageRequest) {
        Page<TeamModel> page = teamRepository.findAll(pageRequest);
        return ResponseEntity.ok(page);
    }

    @PostMapping("/{tid}/participant/{pid}")
    public ResponseEntity addParticipant(@PathVariable("tid") Integer teamId, @PathVariable("pid") Integer partId) {
        teamService.addParticipant(teamId, partId);
        return ResponseEntity.ok().build();
    }

}
