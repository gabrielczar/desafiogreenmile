package com.gabrielczar.desafiogreenmile.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class HomeController {

    @GetMapping("/public")
    public ResponseEntity index() {
        return ResponseEntity.ok("Hello World!");
    }

    @GetMapping("/home")
    public ResponseEntity user() {
        return ResponseEntity.ok().build();
    }
}
