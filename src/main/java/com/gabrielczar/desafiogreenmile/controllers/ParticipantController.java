package com.gabrielczar.desafiogreenmile.controllers;

import com.gabrielczar.desafiogreenmile.models.ParticipantModel;
import com.gabrielczar.desafiogreenmile.repositories.ParticipantRepository;
import com.gabrielczar.desafiogreenmile.services.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/api/participants")
public class ParticipantController {
    private final StorageService storageService;
    private final ParticipantRepository participantRepository;
    private final FileController fileController;

    @Autowired
    public ParticipantController(StorageService storageService, ParticipantRepository participantRepository, FileController fileController) {
        this.storageService = storageService;
        this.participantRepository = participantRepository;
        this.fileController = fileController;
    }

    @PostMapping
    public ResponseEntity save(@RequestBody ParticipantModel participantModel) throws IOException {
        storageService.store(participantModel.getPhoto(), participantModel.getFileContent());
        participantRepository.save(participantModel);
        return ResponseEntity.accepted().body(participantModel);
    }

    @PutMapping
    public ResponseEntity update(@RequestBody ParticipantModel participantModel) throws IOException {
        storageService.store(participantModel.getPhone(), participantModel.getFileContent());
        participantRepository.save(participantModel);
        return ResponseEntity.accepted().body(participantModel);
    }

    @GetMapping
    public ResponseEntity list(Pageable pageRequest) {
        Page<ParticipantModel> page = participantRepository
                .findAll(pageRequest)
                .map(participant -> {
                    participant.setUrl(participant.getPhone() != null ? fileController.loadContentByUrl(participant.getPhoto()) : "");
                    return participant;
                });
        return ResponseEntity.ok(page);
    }

}
