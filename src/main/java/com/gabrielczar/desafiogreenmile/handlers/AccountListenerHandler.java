package com.gabrielczar.desafiogreenmile.handlers;

import com.gabrielczar.desafiogreenmile.models.AccountCredentialsModel;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.persistence.PrePersist;
import javax.transaction.Transactional;

@Component
@Transactional
public class AccountListenerHandler {

    @PrePersist
    public void preSave(final AccountCredentialsModel account) {
        account.setPassword(new BCryptPasswordEncoder().encode(account.getPassword()));
    }

}
