package com.gabrielczar.desafiogreenmile.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.gabrielczar.desafiogreenmile.enumerations.ShirtSize;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.List;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.IDENTITY;

@Setter
@Getter
@ToString(exclude = "team")
@EqualsAndHashCode
@Entity(name = "participants")
public class ParticipantModel {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;

    @NotNull(message = "O nome completo é obrigatório")
    private String fullName;

    @Email(message = "Email inválido")
    private String email;

    private String photo;

    private String phone;

    @Enumerated(STRING)
    @NotNull(message = "O tamanho da camisa é obrigatório")
    private ShirtSize shirtSize;

    @JsonBackReference
    @ManyToMany(mappedBy = "members", cascade = MERGE)
    private List<TeamModel> teams;

    @Transient
    private String fileContent;

    @Transient
    private String url;

    @OneToOne(cascade = ALL)
    private AccountCredentialsModel account;

}
