package com.gabrielczar.desafiogreenmile.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.List;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.GenerationType.IDENTITY;

@Setter
@Getter
@ToString(exclude = "event")
@Entity(name = "organizers")
public class OrganizerModel {
    @Id @GeneratedValue(strategy = IDENTITY)
    private Integer id;

    @NotNull(message = "O nome é obrigatório")
    private String nome;

    @Email(message = "Email inválido")
    private String email;

    private String phone;

    @OneToOne(cascade = ALL)
    private AccountCredentialsModel account;

    @JsonBackReference
    @OneToMany(mappedBy = "organizer", cascade = MERGE)
    private List<EventModel> event;
}
