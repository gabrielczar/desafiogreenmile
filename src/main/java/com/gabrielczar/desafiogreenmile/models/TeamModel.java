package com.gabrielczar.desafiogreenmile.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Set;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.FetchType.EAGER;

@Setter
@Getter
@NoArgsConstructor
@Entity(name = "teams")
public class TeamModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull(message = "O nome da equipe é obrigatório")
    private String name;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private LocalDateTime enrollDate;

    @JsonManagedReference
    @ManyToMany(cascade = MERGE, fetch = EAGER)
    private Set<ParticipantModel> members;

    @JsonManagedReference
    @ManyToOne(cascade = MERGE)
    private EventModel event;

    @Override @JsonIgnore
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TeamModel other = (TeamModel) obj;
        if (name == null) {
            return other.name == null;
        } else return name.equals(other.name);
    }

    @Override @JsonIgnore
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }
}
