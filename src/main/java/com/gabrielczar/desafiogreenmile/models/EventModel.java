package com.gabrielczar.desafiogreenmile.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Set;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.GenerationType.IDENTITY;

@Setter
@Getter
@DynamicInsert
@Entity(name = "events")
public class EventModel {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;

    @NotNull(message = "Nome do evento é obrigatório")
    private String name;

    private String description;

    private String local;

    private LocalDate date;

    @NotNull(message = "Quantidade de participantes por time é obrigatória")
    private Integer numParticipantsByTeam;

    private Integer numTeams;

    private Boolean open = true;

    @JsonBackReference
    @OneToMany(mappedBy = "event", cascade = MERGE)
    private Set<TeamModel> teams;

    @JsonManagedReference
    @ManyToOne(cascade = MERGE)
    private OrganizerModel organizer;

    @JsonIgnore
    public void closeEvent() {
        this.open = false;
    }
}
