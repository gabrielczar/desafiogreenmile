package com.gabrielczar.desafiogreenmile.wrappers;

import com.gabrielczar.desafiogreenmile.models.AccountCredentialsModel;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

import java.util.Date;

public class JwtTokenWrapper {

    private final Claims claims;

    private static class Key {
        private static final String ROLE = "role";
    }

    public JwtTokenWrapper(Claims claims) {
        this.claims = claims;
    }

    public Claims getClaims() {
        return claims;
    }

    public String getUsername() {
        return claims.getSubject();
    }

    public boolean isExpired() {
        return claims.getExpiration().before(new Date());
    }

    public String getRole() {
        return claims.get(Key.ROLE, String.class);
    }

    public static JwtTokenWrapper ofUser(AccountCredentialsModel user) {
        final Claims claims = Jwts.claims();
        claims.put(JwtTokenWrapper.Key.ROLE, user.getAuthority().getAuthority());
        return new JwtTokenWrapper(claims);
    }

}
