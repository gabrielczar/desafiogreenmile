package com.gabrielczar.desafiogreenmile.wrappers;

import com.gabrielczar.desafiogreenmile.models.AccountCredentialsModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class AuthenticationResponseWrapper {
    private final String token;
    private final AccountCredentialsModel account;
}
