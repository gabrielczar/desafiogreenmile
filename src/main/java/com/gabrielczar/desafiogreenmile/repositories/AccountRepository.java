package com.gabrielczar.desafiogreenmile.repositories;

import com.gabrielczar.desafiogreenmile.models.AccountCredentialsModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends CrudRepository<AccountCredentialsModel, Integer> {
    AccountCredentialsModel findByUsername(String username);
}
