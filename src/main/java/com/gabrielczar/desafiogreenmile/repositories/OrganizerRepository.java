package com.gabrielczar.desafiogreenmile.repositories;

import com.gabrielczar.desafiogreenmile.models.OrganizerModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

@RepositoryRestResource(collectionResourceRel = "organizers", path = "organizers")
public interface OrganizerRepository extends CrudRepository<OrganizerModel, Integer> {

    @Override @RestResource(exported = false)
    void deleteById(Integer integer);

    @Override @RestResource(exported = false)
    void delete(OrganizerModel organizerModel);

    @Override @RestResource(exported = false)
    void deleteAll(Iterable<? extends OrganizerModel> iterable);

    @Override @RestResource(exported = false)
    void deleteAll();
}
