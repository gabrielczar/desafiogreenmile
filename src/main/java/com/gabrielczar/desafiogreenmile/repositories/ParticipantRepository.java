package com.gabrielczar.desafiogreenmile.repositories;

import com.gabrielczar.desafiogreenmile.models.ParticipantModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.time.LocalDateTime;

@RepositoryRestResource(collectionResourceRel = "participants", path = "participants")
public interface ParticipantRepository extends JpaRepository<ParticipantModel, Integer> {

    @RestResource(path = "find-by-full-name", rel = "find-by-full-name")
    Page<ParticipantModel> findAllByFullName(@Param("name") String fullName, Pageable pageable);

    @RestResource(exported = false)
    @Query(value = "select * from participants where id in (select distinct members_id from teams t left join teams_members tm on t.id = tm.teams_id where t.event_id = :eid and t.enroll_date >= date :initdate and t.enroll_date < date :enddate)", nativeQuery = true)
    Page<ParticipantModel> findAllByEventAndEnrollDate(@Param("eid") Integer id, @Param("initdate") LocalDateTime initDate, @Param("enddate") LocalDateTime endDate, Pageable pageable);

    @RestResource(path = "find-all-by-event-and-name", rel = "find-all-by-event-and-name")
    @Query(value = "select * from participants p where fullName like '%:name%' and id in (select distinct members_id from teams t left join teams_members tm on t.id = tm.teams_id where t.event_id = :event)", nativeQuery = true)
    Page<ParticipantModel> findAllByEventAndName(@Param("event") Integer eventId, @Param("name") String name, Pageable pageable);

    @Override @RestResource(exported = false)
    void deleteInBatch(Iterable<ParticipantModel> iterable);

    @Override @RestResource(exported = false)
    void deleteAllInBatch();

    @Override @RestResource(exported = false)
    void deleteById(Integer integer);

    @Override @RestResource(exported = false)
    void delete(ParticipantModel participantModel);

    @Override @RestResource(exported = false)
    void deleteAll(Iterable<? extends ParticipantModel> iterable);

    @Override @RestResource(exported = false)
    void deleteAll();
}
