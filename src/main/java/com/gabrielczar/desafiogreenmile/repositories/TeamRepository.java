package com.gabrielczar.desafiogreenmile.repositories;

import com.gabrielczar.desafiogreenmile.models.TeamModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.time.LocalDateTime;

@RepositoryRestResource(collectionResourceRel = "teams", path = "teams")
public interface TeamRepository extends JpaRepository<TeamModel, Integer> {

    @RestResource(path = "find-by-event-and-enroll-date", rel = "find-by-event-and-enroll-date")
    Page<TeamModel> findAllByEvent_IdAndEnrollDateBetween(@Param("event") Integer eventId, @Param("init") LocalDateTime initDate, @Param("end") LocalDateTime endDate, Pageable pageable);

    @RestResource(path = "find-by-event-and-date", rel = "find-by-event-and-date")
    Page<TeamModel> findAllByEnrollDateBetween(@Param("init") LocalDateTime initDate, @Param("end") LocalDateTime endDate, Pageable pageable);

    @RestResource(path = "find-by-event-and-name", rel = "find-by-event-and-name")
    Page<TeamModel> findAllByEvent_IdAndName(@Param("event") Integer eventId, @Param("name") String name, Pageable pageable);

    @RestResource(path = "find-by-name", rel = "find-by-name")
    Page<TeamModel> findAllByName(@Param("name") String name, Pageable pageable);

    @Override @RestResource(exported = false)
    void deleteInBatch(Iterable<TeamModel> iterable);

    @Override @RestResource(exported = false)
    void deleteAllInBatch();

    @Override @RestResource(exported = false)
    void deleteById(Integer integer);

    @Override @RestResource(exported = false)
    void delete(TeamModel teamModel);

    @Override @RestResource(exported = false)
    void deleteAll(Iterable<? extends TeamModel> iterable);

    @Override @RestResource(exported = false)
    void deleteAll();
}
