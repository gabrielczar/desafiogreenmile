package com.gabrielczar.desafiogreenmile.repositories;

import com.gabrielczar.desafiogreenmile.models.EventModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "events", path = "events")
public interface EventRepository extends JpaRepository<EventModel, Integer> {

    @RestResource(path = "find-all-by-name", rel = "find-all-by-name")
    List<EventModel> findAllByName(@Param("name") String name);

    @Override @RestResource(exported = false)
    void deleteInBatch(Iterable<EventModel> iterable);

    @Override @RestResource(exported = false)
    void deleteAllInBatch();

    @Override @RestResource(exported = false)
    void deleteById(Integer integer);

    @Override @RestResource(exported = false)
    void delete(EventModel eventModel);

    @Override @RestResource(exported = false)
    void deleteAll(Iterable<? extends EventModel> iterable);

    @Override @RestResource(exported = false)
    void deleteAll();
}
